package einfinf8.mwallewein;

import java.util.Date;
import java.text.SimpleDateFormat;

public class TernarySearch {   
	   static final int UNDEF = -1;
	   
	   static int binaryCount = 0;

	   public static int find ( int [] a, int x) {
	      return _find (a ,0,a. length -1,x);
	   }

	   private static int _find ( int [] a,int l,int r, int x) {
	      binaryCount++;
		   if (l>r) return UNDEF ;
	      int m=(l+r) /2;
	      if (x==a[m]) return m;
	      else if (x<a[m]) return _find (a,l,m -1,x);
	      else return _find (a,m+1,r,x);
	   }

	   public static int ternaryRec(int a[], int x) {
		   
		   // Feld dritteln
		   int pos1 = a.length / 3;
		   int pos2 = pos1 * 2;
		   
		   // Prüfen ob es bereits mit einer Grenze passt
		   if(pos1 == x)
			   return pos1;
		   else if(pos2 == x)
			   return pos2;
		   
		   // Jeweils auf die Grenzen verteilen
		   if(pos1 > x)
			   return _find(a, 0, pos1 - 1, x);
		   else if(pos2 > x)
			   return _find(a, pos1 + 1, pos2 - 1, x);
		   else
			   return _find(a, pos2 + 1, a.length, x);
	   }
	   
	   public static void main(String[] args) {
	      int[] testArray = new int[10000];
	      for(int i = 0; i < 10000; i++)
	      {
	    	  if(i % 2 != 0)
	    		  testArray[i] = i * (-1);
	    	  else
	    		  testArray[i] = i;
	      }

	      // Zum Zeit messen...
	      long time1, time2;
	      int index;
	      long timeDiff;
	      Date diff;
	      SimpleDateFormat sdf = new SimpleDateFormat("ss.SSS");
	      
	      // Zeitdifferenz merken zwischen vorher und nachher
	      time1 = System.currentTimeMillis();
	      index = ternaryRec(testArray, 4320);
	      time2 = System.currentTimeMillis();
	      timeDiff = time2 - time1;
	      diff = new Date(timeDiff);
	      
	      System.out.println("Find 4320 in testArray with ternaryRec: index=" +
	    		  index);
	      
	      System.out.println("Zeit in Sekunden.Millisekunden: " + sdf.format(diff)); // 01.107
	      System.out.println("Anzahl Durchgänge: " + binaryCount); // 8
	      binaryCount = 0;
	      
	      time1 = System.currentTimeMillis();
	      index = _find(testArray, 0, testArray.length, 4320);
	      time2 = System.currentTimeMillis();
	      timeDiff = time2 - time1;
	      diff = new Date(timeDiff);
	      
	      System.out.println("Find 4320 in testArray with binaryRec: index=" +
	    		  index);
	      
	      System.out.println("Zeit in Sekunden.Millisekunden: " + sdf.format(diff)); // 00.582
	      System.out.println("Anzahl Durchgänge: " + binaryCount); // 10
	   }
	}
