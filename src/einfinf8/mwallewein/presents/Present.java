package einfinf8.mwallewein.presents;

abstract class Present{
   abstract double surface();
   abstract double computeRibbonLength();
   
   double computePaperSize(){  // same for all presents
	   return surface() * 1.5;  // 50% waste
   }		
}