package einfinf8.mwallewein.presents;

class Cylinder extends Present {
	double height; // in meter
	double diameter; // in meter

	public Cylinder(double height, double diameter)
	{
		this.height = height;
		this.diameter = diameter;
	}
	
	// TODO: surface, computeRibbonLength
	@Override
	double surface() {
		// Umfang Kreis: 2 * PI * r
		double umf = this.diameter * Math.PI;
		double grundfl = Math.pow(this.diameter / 2, 2) * Math.PI;
		
		// GrundFläche für oben und unten Plus die Fläche rings rum
		return grundfl * 2 + umf * this.height; 
	}

	@Override
	double computeRibbonLength() {
		// Wie Umfang
		return this.diameter * Math.PI * 1.10;
	}
}
