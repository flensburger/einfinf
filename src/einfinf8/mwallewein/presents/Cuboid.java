package einfinf8.mwallewein.presents;

class Cuboid extends Present {
	double length; // in meter
	double width; // in meter
	double height; // in meter

	public Cuboid(double length, double width, double height) {
		this.height = height;
		this.width = width;
		this.length = length;
	}
	
	// TODO: surface, computeRibbonLength
	@Override
	double surface() {
		return this.height * this.length * this.width;
	}

	@Override
	double computeRibbonLength() {
		return this.length * this.width * 1.10; 
	}
}
