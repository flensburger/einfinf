package einfinf8.mwallewein.presents;

public class ChristmasPresents {

	public static void main(String[] args) {
		/*
		 * Schreiben Sie eine Testklasse ChristmasPresents, in dem Sie verschiedene 
		 * Present-Objekte generieren: 
		 * z.B. fünf Weinflaschen, 
		 * 		zwei (kugelrunde) Geschenkkörbe, 
		 * 		einen Gartenzwerg, 
		 * 		einen Teddy 
		 * 		und drei Computerspiele. 
		 * 
		 * Legen Sie die konkreten Maße dieser Geschenke fest. 
		 */
		
		Cylinder[] wines = 
			{ 
				new Cylinder(0.2, 0.05),
				new Cylinder(0.2, 0.05),
				new Cylinder(0.4, 0.05),
				new Cylinder(0.4, 0.05),
				new Cylinder(0.2, 0.05)
			};
		
		Ball basket1 = new Ball(0.4);
		Ball basket2 = new Ball(0.4);
		
		Cuboid[] computerGames = 
			{
				new Cuboid(0.1, 0.02, 0.25),
				new Cuboid(0.1, 0.02, 0.25),
				new Cuboid(0.1, 0.02, 0.25)
			};
		
		Cuboid dwarf = new Cuboid(0.3, 0.5, 0.8);
		Cuboid teddy = new Cuboid(0.3, 0.5, 0.8);
		
		double paperSize = 0;
		double ribbonSize = 0;
		
		for(Cylinder c : wines)
		{
			paperSize += c.computePaperSize();
			ribbonSize += c.computeRibbonLength();
		}
		
		paperSize += basket1.computePaperSize();
		paperSize += basket2.computePaperSize();
		ribbonSize += basket1.computeRibbonLength();
		ribbonSize += basket2.computeRibbonLength();
		
		
		for(Cuboid c : computerGames)
		{
			paperSize += c.computePaperSize();
			ribbonSize += c.computeRibbonLength();
		}
		
		paperSize += dwarf.computePaperSize();
		paperSize += teddy.computePaperSize();
		ribbonSize += dwarf.computeRibbonLength();
		ribbonSize += teddy.computeRibbonLength();
		
		// Aufrunden
		ribbonSize = Math.ceil(ribbonSize);
		paperSize = Math.ceil(paperSize);
		
		// Geschenkpapier auf 5m² berechnen
		double paperPrice = ((int)paperSize / 5 * 10.0) + (paperSize % 5 > 0 ? 10 : 0);
		
		System.out.println("Papierbedarf: " + paperSize + "m^2");
		System.out.println("Preis Geschenkpapier: " + paperPrice + "€");
		
		System.out.println("Schleifenbedarf: " + ribbonSize + "m");
		System.out.println("Preis Schleifen: " + ribbonSize * 3 + "€");
		
	}
}
