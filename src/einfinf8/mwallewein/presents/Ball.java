package einfinf8.mwallewein.presents;

class Ball extends Present {
	double radius; // in meter

	// TODO: surface, computeRibbonLength

	public Ball(double radius)
	{
		this.radius = radius;
	}
	
	@Override
	double surface() {
		// Oberfläche Kugel: 4 * r² * PI 
		return 4 * Math.pow(this.radius, 2) * Math.PI;
	}

	@Override
	double computeRibbonLength() {
		// Umfang: 2 * PI * r
		return 2 * Math.PI * this.radius * 1.10;
	}
}