package einfinf8.mwallewein;
/********************************************************
Algorithmus:

	f(0,1).
	X>0 AND f(X-1,Y) -> f(X,2*Y).
	
	--> überprüft ob y eine zweierpotenz mit x ist
		2^x = y
		
		
Ergebnisse der Anfragen:
	
	f(X,64) -> 64/2 = 32/2 = 16 / 2 = 8 / 2 = 4 / 2 = 2 / 2 = 1
				-1 		-2 		-3  	-4 		-5 		-6		-7
				
				X = 7
				
	f(X,67) -> Error
	f(3, Y) -> Y = 8
********************************************************/
public class DeductiveAlgorithm {

}
