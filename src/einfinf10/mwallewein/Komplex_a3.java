package einfinf10.mwallewein;


public class Komplex_a3 {

	public static void main(String[] args) {
		long[][] Data = new long[6][9];
		for (int i = 1; i < 10; i++) {
			for (int k = 0; k < 6; k++) {
				if (k == 0) {
					Data[k][i - 1] = i;
					
				}
				if (k == 1) {
					Data[k][i - 1] = ((int) (Math.log(getCurrent(i))));
				}
				if (k == 2) {
					Data[k][i - 1] = getCurrent(i) * ((int) Math.log(getCurrent(i)));
				}
				if (k == 3) {
					Data[k][i - 1] = (getCurrent(i) * getCurrent(i));
				}
				if (k == 4) {
					Data[k][i - 1] = (getCurrent(i) * getCurrent(i) * getCurrent(i));
				}
				if (k == 5) {
					long help = 2;
					for (int h = 0; h < getCurrent(i); h++) {
						help = help * 2;
					}
					Data[k][i - 1] = help;
				}
			}
		}
		for (int q = 0; q < 9; q++) {
			for (int r = 0; r < 6; r++) {
				System.out.print(Data[r][q] + "ns        ");
			}
			System.out.println("");
		}

	}
	
	public static int getCurrent(int i){
		int help = 2;
		for(int k=0;k<i;k++){
			help = help * 2;
		}
		return help;
	}

}
//
//  t1(n) = ld n -> fast pot,
//
//  t2(n) = n -> Selection-Sort, Quersumme,linear Search 
//
// t3(n) = n*ld n -> Binary Search, Quicksort, Mergesort
//
// t4(n) = n^2 ->  Bubblesort, Insertionsort
//
// t5(n) = n^3 ->
//
// t6(n) = 2^n -> Fibbonaci,
//


