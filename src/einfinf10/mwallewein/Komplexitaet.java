package einfinf10.mwallewein;

/**
 * A1:   t1(n) = ld n   (ld n = log2 n) ->  fast pot, Binary Search, Quersumme (log10)     	
 * A2:   t2(n) = n        				->  linear Search, Fibbonaci(mit Vorgänger speichern)	
 * A3:   t3(n) = n*ld n       			->  Quicksort, Mergesort
 * A4:   t4(n) = n² 					->	Bubblesort, Insertionsort, Selection-Sort
 * A5:   t5(n) = n³ 					->  
 * A6:   t6(n) = 2^n 					->  Fibbonaci (Rekursiv, genau: 1.6^n gold. Schnitt) 
 */

public class Komplexitaet {
	
	public static void main(String[] args) {
		// 6 Zeilen, 9 Spalten
		long[][] results = new long[6][9];
		int base = 2; 
		
		for(int i = 0; i < 9; i++)
		{
			int row = 0;
			// A1
			results[row++][i] = (long)Math.log(Math.pow(base, i + 1));
			
			// A2
			results[row++][i] = (long)Math.pow(base, i + 1);
			
			// A3
			results[row++][i] = (i + 1) * (long)Math.log(Math.pow(base, i + 1));
			
			// A4
			results[row++][i] = (long)Math.pow(Math.pow(base, i + 1), 2);
			
			// A5
			results[row++][i] = (long)Math.pow(Math.pow(base, i + 1), 3);
			
			// A6
			results[row++][i] = (long)Math.pow(2, Math.pow(base, i + 1));
		}
		
		System.out.print("A# ");
		for(int i = 0; i < 9; i++)
		{
			System.out.print(fillUpWithChars("k=2^" + i, ' ', 20));
		}
		
		System.out.println();
		
		for(int i = 0; i < results.length; i++)
		{
			System.out.print("A" + i + " ");
			for(int j = 0; j < results[i].length; j++)
			{
				System.out.print(fillUpWithChars(results[i][j] + "", ' ', 20));
			}
			System.out.println("");
		}
		
	}
	
	/**
	 * Füllt einen String auf eine bestimmte Länge mit einem bestimmten Char auf
	 * @param param 
	 * @param character
	 * @param length
	 * @return
	 */
	private static String fillUpWithChars(String param, char character, int length)
	{
		for(int i = param.length(); i < length; i++)
			param += character;
		
		return param;
	}
}
