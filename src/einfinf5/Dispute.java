package einfinf5;

import java.util.ArrayList;
import java.util.Arrays;

public class Dispute {
   public static int marge(int n) {
	   if (n == 0){
		   return 1;
	   } else {
		   return n - homer(marge(n-1));
	   }
   }

   public static int homer(int n) {
	   if (n == 0){
		   return 0;
	   } else {
		   return n - marge(homer(n-1));
	   }
   }

   public static boolean dispute(int n) {
	   return !(marge(n) == homer(n));
   }

   private static int[] toArray(ArrayList<Integer> list){
	   int[] retList = new int[list.size()];
	   for(int i = 0; i < retList.length; i++){
		   retList[i] = list.get(i);
	   }
	   return retList;
   }
   
   public static int[] differenceDisputes(int x) {
	   ArrayList<Integer> list = new ArrayList<Integer>();
	   
	   int lastDispute = 0;
	   int count = 1 ;
	   while (list.size() < x){
		   if (dispute(count)){
			   list.add(count - lastDispute);
			   lastDispute = count;
		   }
		   count++;
	   }
	   return toArray(list);
   }

   public static void main(String args[]) {
	   System.out.println(Arrays.toString(differenceDisputes(20)));
   }
}