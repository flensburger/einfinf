package einfinf5;

public class Clock {
	private int h;
	private int min;

	public int getMin() {
		return min;
	}

	public int getH() {
		return h;
	}

	// hier bitte verschieden Konstruktoren einfuegen
	
	private Clock(Clock c){
		h = c.getH();
		min = c.getMin();
		this.normalize();
	}
	
	public Clock(String input){
		String hours, minutes;
		hours = input.substring(0,input.indexOf(":"));
		minutes = input.substring(input.indexOf(":")+1);
		
		h = Integer.parseInt(hours);
		min = Integer.parseInt(minutes);
		this.normalize();
		
	}
	public Clock(int minutes){
	
		h = minutes%60;
		min = (int) minutes/60;
		this.normalize();
		
	}
	public Clock(int hours, int minutes){
		
		h = hours;
		min = minutes;
		this.normalize();
	}
	
	public Clock add(int min) {
		Clock ret = new Clock(this);
		ret.min += min;
		ret.normalize();
		return ret;
	}

	public Clock add(Clock c) {
		Clock ret= new Clock(this);
		ret.h += c.getH();
		ret.min += c.getMin();
		ret.normalize();
		return ret;
			
	}

	public String toString() {
		return ("Clock ID: " + this.getClass() + " | " + h + ":" + min);

	}
	
	private void normalize(){
		
		if (min >= 60){
			h += min/60;
			min = min%60;
		}
		
		if (h >= 24){
			h = h%24;
		}
		
		
	}

	public static void main(String[] args) {
		

	}
}
