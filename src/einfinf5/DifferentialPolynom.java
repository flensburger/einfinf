package einfinf5;

public class DifferentialPolynom {
	public static double[] diffPoly(double[] coeffs) {
		double[] ret = new double[coeffs.length - 1];

		for (int i = 1; i < coeffs.length; i++) {
			ret[i - 1] = coeffs[i] * i;
		}

		return ret;
	}

	public static double[] diffPoly(double[] coeffs, int n){
		if (n > coeffs.length){
			return new double[]{0.0};

		}
		double[] ret = coeffs.clone();
		for (int i = 0; i < n; i++){
			ret = diffPoly(ret);
		}
		return ret;
	}
	
	public static double[] diffPoly2(double[] coeffs, int n){
		if(n == 0){
			return coeffs;
		} else {
			return diffPoly2(diffPoly(coeffs), n - 1);
		}
	}
	
	
	public static String toFkt(double[] inp) {
		String ret = "";

		for (int i = inp.length - 1; i >= 0; i--) {
			if (i == 0) {
				ret += inp[i];
			} else {
				ret += inp[i] + "x^" + i + " + ";
			}
		}

		return ret;
	}

	public static void main(String[] args) {

		double[] input = { 4.0, 2.0, 1.0 };
		System.out.println(toFkt(input));
		System.out.println(toFkt(diffPoly(input)));
		System.out.println(toFkt(diffPoly(input, 1)));
		System.out.println(toFkt(diffPoly(input, 2)));
		System.out.println(toFkt(diffPoly(input, 1)));
		System.out.println(toFkt(diffPoly2(input, 2)));
	}
}