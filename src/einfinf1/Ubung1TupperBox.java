package einfinf1;

public class Ubung1TupperBox {
	public static void main(String[] args) {
		cubicHeron(16, 50);
	}

	public static boolean cubicHeron(int volume, int anzahlDerDurchgaenge) {

		float a, b, c;
		a = volume;
		b = 1;
		c = 1;

		for (int i = 0; i < anzahlDerDurchgaenge; i++) {
			
			/* Wir führen den Algorythmus auf zwei Seiten aus.
			 * Bei der Berechnung von b muss ein zweiter Teiler hinzugefügt werden, 
			 * weil das Volume ja durch a*b*c berechnet wird. Durch Umstellen kommt 
			 * man dann auf b = volume / a / c
			 * 
			 *  Es müssen allerdings mehr Operationen als im 2D Raum ausgeführt werden.
			 */
			
			a = (a + b) / 2;
			b = volume / a / c;

			
			a = (a + c) / 2;
			c = volume / a / b;
		}
		System.out.println("a: " + a + " b: " + b + " c: " + c);

		return false;
	}
}
