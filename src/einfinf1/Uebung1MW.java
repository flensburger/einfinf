package einfinf1;

public class Uebung1MW {
	public static void main(String[] args) {
		cubicHeron(16, 50);
		System.out.println(cubicHeron(27, 50));
		
		int primeTest = 21;
		
		for(int i = 2; i < 200; i++)
		{
//			// System.out.println(String.format("Zahl %d: Nächste Primzahl? ->%s",i, nextPrime(i)));
//			// System.out.println(String.format("Zahl %d ist eine Primzahl? ->%s",i, isPrime(i)));
			System.out.println(String.format("Zahl %d ist eine Primzahl? ->%s. Nächste Primzahl%d",i, isPrime(i), nextPrime(i)));
		}
		
		System.out.println(String.format("Zahl %d ist eine Primzahl? ->%s. Nächste Primzahl%d",1, isPrime(1), nextPrime(1)));
		
		
	}

	public static float cubicHeron(int volume, int anzahlDerDurchgaenge) {

		// Ein Quader mit den Längen volume, 1, 1 definieren und
		// die beiden Seiten mit Länge == 1 mit jedem Schritt der
		// Länge von a angleichen
		float a, b, c;
		a = volume;
		b = 1;
		c = 1;
		
		// Die Berechnung erfolgt in zwei schritten...
		for (int i = 0; i < anzahlDerDurchgaenge; i++) {
			
			// ...Erst wird eine Seite des Quaders verändert, analog
			// zu dem 2D Algorithmus...
			a = (a + b) / 2;
			b = volume / a / c;

			// ...Danach wird die letzte, noch übrige Seite verändert
			a = (a + c) / 2;
			c = volume / a / b;
		}
		
		// Da a, b, c gleichlang sein "sollten", einfach a zurückgeben
		return a;
	}
	
	/**
	 * Prüft ob die eingegebene Zahl eine Primzahl ist
	 * @param n zu überprüfende Zahl
	 * @return true wenn Primzahl
	 */
	public static boolean isPrime(int n)
	{ 
		// alles unter 1 extra handlen, da sonst immer false zurückgegeben wird
		// in der Schleife unten
		if(n < 2)
			return false;
		
		// Bis zur Wurzel von n durchgehen und n durch diese Zahlen
		// teilen
		int maxIndex = (int)Math.sqrt((double)(n));
		for(int i = 2; i <= maxIndex; i++)
		{	
			// Wenn sich ein Rest bei irgendeiner Zahl ergibt
			// dann ist n keine Primzahl
			if(n % i == 0)
				return false;
		}
		
		// Wenn sich kein Rest ergeben hat, dann ist
		// n eine Primzahl
		return true;
	}
	
	/**
	 * Gibt die nächste Primzahl >= n zurück
	 * @param n Anfangswert
	 * @return Gibt die nächste Primzahl >= n zurück
	 */
	public static int nextPrime(int n)
	{
		// Bei n anfangen
		boolean isPrime = isPrime(n);
		
		// Solange isPrime nicht true zurückgibt,
		// n um 1 erhöhen
		while(!isPrime)
			isPrime = isPrime(++n);
		
		return n;
	}
}
