package einfinf7.jwagner.geo;

public interface Movable {
	void translate(int dx, int dy);
}
