package einfinf7.jwagner.geo;

public class Line implements Movable {
	Point start;
	Point end;
	Point diff;
	
	public Line(Point p, Point q){
		this.start = p;
		this.end = q;
		diff = new Point(p.x - q.x, p.y - q.y);
	}

	@Override
	public void translate(int dx, int dy) {
		// Verschiebt die ganze Linie in zwei Richtungen
		start.x += dx;
		end.x += dx;
		
		start.y += dy;
		end.x = dy;
		
	}
	
	public double length(){
		return Math.sqrt((diff.x * diff.x)+(diff.y * diff.y));
	}
	
	public String toString(){
		return "Line: " + start.toString() + " - " + end.toString() + " - L�nge:" + this.length();
	}

}