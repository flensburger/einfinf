package einfinf7.jwagner.geo;

import java.util.Arrays;

class GeomTest {
   public static void main(String[] args) {
      Point p1 = new Point(4,0);
      Point p2 = new Point(0,0);
      Point p3 = new Point(0,4);
      
      Line l1 = new Line(p1, p2);
      Line l2 = new Line(p2, p3);
      Line l3 = new Line(p3, p1);
      
      Triangle tri = new Triangle(p1, p2, p3);
      Object[] all = {p1, p2, p3, l1, l2, l3, tri};
      
      System.out.println(Arrays.toString(all));
      
   }
}