package einfinf7.jwagner.geo;

public class Point implements Movable {
	int x;
	int y;

	public Point(int i, int j) {
		this.x = i;
		this.y = j;
	}

	// TODO: Constructor, toString, distance, translate
	@Override
	public void translate(int dx, int dy) {
		// Verschiebung
		x += dx;
		y += dy;

	}
	
	public String toString() {
		return "Point: x=" + x + " y=" + y;
	}

	public double distance(Point p) {
		Line line = new Line(this, p);
		return line.length();
	}
	
	

}