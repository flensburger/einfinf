package einfinf7.jwagner.geo;

public class Triangle implements Movable {
	Point a;
	Point b;
	Point c;
	Point[] all;

	// TODO: Constructor, toString, perimeter, translate
	public Triangle(Point a, Point b, Point c) {
		this.a = a;
		this.b = b;
		this.c = c;
		all = new Point[3];
		all[0] = a;
		all[1] = b;
		all[2] = c;
	}

	@Override
	public void translate(int dx, int dy) {
		for (int i = 0; i < all.length; i++) {
			all[i].x += dx;
			all[i].y += dy;
		}

	}

	public double perimeter() {
		double sum = 0;
		for (int i = 0; i < all.length; i++) {
			Line l = new Line(all[i], all[followingCount(i)]);
			sum += l.length();
		}
		return sum;
	}

	public int followingCount(int x) {
		if (x == all.length-1) {
			return 0;
		} else {
			return x + 1;
		}
	}

	public String toString() {
		return ("Triangle: \n" + a.toString() + "\n" + b.toString() + "\n" + c.toString() + "\n" + "Perimeter: "
				+ perimeter());
	}
}
