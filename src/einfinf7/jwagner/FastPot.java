package einfinf7.jwagner;

/********************************************************
 * Hier bitte Antwort zu Teil a) als Kommentar einfuegen
 ********************************************************/

public class FastPot {
	public static double fastPotRek(double x, int n) {
		if (n == 0) {
			return 1;
		} else if (n % 2 == 0) {
			return fastPotRek(x * x, n / 2);
		} else {
			return x * fastPotRek(x, n - 1);
		}
	}

	public static double fastPotIter(double x, int n) {
		double sum = 1.0;
		for (int i = 0; i < n; i++) {
			sum *= x;
		}
		return sum;

	}

	public static void main(String[] args) {
		for (int i = 1; i < 14; i++) {
			System.out.println(i + ": " + fastPotRek(2, i));
		}
	}
}