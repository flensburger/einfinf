package einfinf7.mwallewein.geo;

public class GeomTest
{
	public static void main(String[] args) {
		Point p1 = new Point(3, 7);
		p1.translate(5, 7);
		
		System.out.println(p1.toString());
		
		Point p2 = new Point(-5, 6);
		
		System.out.println("Dist p1<->p2: " + p1.distance(p2));
		
		Line line = new Line(p1, p2);
		
		System.out.println(line.toString());
		line.translate(4, 3);
		System.out.println(line.toString());
		
		Triangle tri = new Triangle(p1, p2, new Point(54, 2));
		System.out.println(tri.toString());
		
		System.out.println("Umfang: " + tri.perimeter());
		
	}
}
