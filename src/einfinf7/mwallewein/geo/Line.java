package einfinf7.mwallewein.geo;

public class Line implements Movable
{
	Point pStart, pEnd;
	
	@Override
	public void translate(int dx, int dy) {
		this.pStart.setX(this.pStart.getX() + dx);
		this.pStart.setY(this.pStart.getY() + dy);
		
		this.pEnd.setX(this.pEnd.getX() + dx);
		this.pEnd.setY(this.pEnd.getY() + dy);
	}
	
	public Line(Point p1, Point p2)
	{
		this.pStart = p1;
		this.pEnd = p2;
	}
	
	public double length()
	{
		return this.pStart.distance(this.pEnd);
	}
	
	public String toString()
	{
		return String.format("{%s,%s}", 
				this.pStart.toString(), 
				this.pEnd.toString());
	}

	public Point getpStart() {
		return pStart;
	}

	public void setpStart(Point pStart) {
		this.pStart = pStart;
	}

	public Point getpEnd() {
		return pEnd;
	}

	public void setpEnd(Point pEnd) {
		this.pEnd = pEnd;
	}
}
