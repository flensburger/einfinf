package einfinf7.mwallewein.geo;

public class Triangle implements Movable {
	Point p1, p2, p3;

	public Triangle(Point p1, Point p2, Point p3) {
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}
	
	@Override
	public void translate(int dx, int dy) {
		this.p1.setX(this.p1.getX() + dx);
		this.p1.setY(this.p1.getY() + dy);

		this.p2.setX(this.p2.getX() + dx);
		this.p2.setY(this.p2.getY() + dy);

		this.p3.setX(this.p3.getX() + dx);
		this.p3.setY(this.p3.getY() + dy);

	}

	public double perimeter() {
		double per = p1.distance(p2);
		per += p1.distance(p3);
		per += p2.distance(p3);

		return per;
	}

	public String toString() {
		return String.format("{%s,%s,%s}", this.p1.toString(),
				this.p2.toString(), this.p3.toString());
	}

	public Point getP1() {
		return p1;
	}

	public void setP1(Point p1) {
		this.p1 = p1;
	}

	public Point getP2() {
		return p2;
	}

	public void setP2(Point p2) {
		this.p2 = p2;
	}

	public Point getP3() {
		return p3;
	}

	public void setP3(Point p3) {
		this.p3 = p3;
	}

}
