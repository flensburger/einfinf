package einfinf7.mwallewein.geo;

public class Point implements Movable{
	private int x, y;
	
	public Point(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	@Override
	public void translate(int dx, int dy)
	{
		this.x += dx;
		this.y += dy;
	}
	
	public double distance(Point p)
	{
		// Anhand vom Satz des Pythagoras berechnen
		int yDiff, xDiff;
		if(this.y >= p.getY())
			yDiff = this.y - p.getY();
		else
			yDiff = p.getY() - this.y;
		
		if(this.x >= p.getX())
			xDiff = this.x - p.getX();
		else
			xDiff = p.getX() - this.x;
			
		return Math.sqrt((double)(yDiff * yDiff + xDiff * xDiff)); 
		
	}
	
	public String toString()
	{
		return String.format("[%d,%d]", this.x, this.y);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
}
