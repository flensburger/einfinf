package einfinf7.mwallewein;

/********************************************************
a)

5. fly home
7. teleport to 0
8. run faster to 0
9  run away

b)
 Überschreiben bedeutet eine aus einer Basisklasse oder einem Interface definierten
 Methode mit der gleichen Signatur in einer ableitenden Klasse zu ersetzen.
 
 Überladen bedeutet eine Methode mit gleichem Namen wie eine bestehende 
 aber mit anderer Signatur/Parametern zu definieren. Hierbei wird im 
 Gegensatz zur Überschreibung keine Methode ersetzt.
 
 c)
 2) -> Es kann kein Konstruktor einer Abstrakten Klasse aufgerufen werden
 4) -> RoadRunner und Fish sind unterschiedliche Unterklassen von Animal
 	   und können nicht gegeneinander zugewiesen werden, da sie evtl. untersch
 	   Klassensignaturen haben
 
********************************************************/

public class Animal {

}
