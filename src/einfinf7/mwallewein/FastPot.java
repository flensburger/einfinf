package einfinf7.mwallewein;

/********************************************************
Hier bitte Antwort zu Teil a) als Kommentar einfuegen
fastPot(2, 13)
	2 * fastPot(2, 12)
	2 * 	fastPot(2 * 2, 6)
	2 * 		fastPot(8, 3)
	2 * 		 8 * fastPot(8, 2)
	2 * 		 8 * 	fastPot(8 * 8, 1)
	2 *  2 * 4 * 8 * 8 * 16 * fastPot(16, 0) 		

********************************************************/
public class FastPot {
	
	private static int rekCount = 0;
	private static int iterCount = 0;
	
 public static double fastPotRek(double x, int n){
	 rekCount++;
	 if(n == 0)
		 return 1;
	 if(n % 2 == 0 && n > 1)
		 return fastPotRek(x * x, n / 2);
	 else
		 return x * fastPotRek(x, n- 1);
 }

 public static double fastPotIter(double x, int n){
	 double result = 1;
	 for(int i = 0; i < n; i++)
	 {
		 result *= x;
		 iterCount++;
	 }
	 
	 return result;

 }

 public static void main(String[] args) {
	 System.out.println(fastPotRek(2, 13));
	 System.out.println(fastPotIter(2, 13));
	 
	 System.out.println("Rek Anzahl: " + rekCount); // 7
	 System.out.println("Iter Anzahl: " + iterCount); // 13
 }
}