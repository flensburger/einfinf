package einfinf4;

import java.math.BigInteger;

public class Ackermann {
	public static void main(String[] args){
		System.out.println(ackermann(BigInteger.valueOf(1), BigInteger.valueOf(10)));
		System.out.println(ackermann(BigInteger.valueOf(2), BigInteger.valueOf(4)));
		System.out.println(ackermann(BigInteger.valueOf(3), BigInteger.valueOf(3)));
	}
	
	/*
	 * Ergebnisse:
	 * 
	 * 1024
	 * 65536
	 * 65536
	 */
	public static BigInteger ackermann(BigInteger x, BigInteger y){
		if (y.equals(BigInteger.ZERO)){
			return BigInteger.ZERO;
		}
		if (x.equals(BigInteger.ZERO)){
			return y.multiply(BigInteger.valueOf(2));
		}
		if (y.equals(BigInteger.ONE)){
			return BigInteger.valueOf(2);
		}
		return ackermann(x.subtract(BigInteger.ONE), ackermann(x, y.subtract(BigInteger.ONE)));
	}
}
