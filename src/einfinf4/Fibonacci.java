package einfinf4;

import java.math.BigInteger;

public class Fibonacci {
	public static void main(String[] args) {
		for (int i = 1; i < 15; i++) {
			System.out.println(fib1(i).toString());
			System.out.println(fib2(i).toString());
			System.out.println("-");
		}
	}

	public static BigInteger fib1(int x) {
		
		if (x == 0 || x == 1) {
			/*
			 * Kann durch x <= 1 ersetzt werden.
			 */
			return BigInteger.ONE;
		} else {

			return fib1(x - 2).add(fib1(x - 1));
		}
	}
	/*
	 * fib(5)
	 * 
	 * fib(3) | fib(4)
	 * 
	 * fib(1) fib (2) | fib(2) fib(3)
	 * 
	 * 1 1+1 1+1 | fib(2) +1
	 * 
	 * 1 1+1 1+1 | 2 + 1
	 * 
	 * 8
	 */

	public static BigInteger fib2(int x) {
		BigInteger[] fibonacci;
		if (x < 2) {
			fibonacci = new BigInteger[3];
		} else {
			fibonacci = new BigInteger[x+1];
		}
		fibonacci[0] = BigInteger.ONE;
		fibonacci[1] = BigInteger.ONE;

		for (int i = 2; i <= x; i++) {
			fibonacci[i] = fibonacci[i - 1].add(fibonacci[i - 2]);
		}
		return fibonacci[x];
	}

}
