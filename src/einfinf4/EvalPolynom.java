package einfinf4;

public class EvalPolynom {
	
   public static double evalSimple(double[] a, double x){
        double sum = a[0];
        double quadX=x;
        for(int i = 1; i < a.length; i++){
        	sum += a[i]*quadX;
        	quadX *= x;
        }
        return sum;
    
   }
  
   public static void main (String args[]) {
	   double[] input = {1.0,2.0,3.0,4.0,5.0};
       System.out.println(evalSimple(input, 5)); 
	   
   }
}
