package einfinf12.mwallewein;

public class ComplexNumber {
	private class InvalidInputForSquareRoot extends RuntimeException {
		// hier bitte Quelltext einfuegen
	}

	private double real;
	private double imag;

	public ComplexNumber(double re, double im) {
		this.real = re;
		this.imag = im;
	}

	public ComplexNumber add(ComplexNumber k) {
		// Einfach draufaddieren
		return new ComplexNumber(k.real + this.real, k.imag + this.imag);
	}

	public ComplexNumber mult(ComplexNumber k) {
		// Gemäß Schema (a + ib) * (c + id) = (ac-bd) + i(bc + ad)
		return new ComplexNumber(this.real * k.real - this.imag * k.imag,
				this.imag * k.real + this.real * k.imag);
	}

	public double absoluteValue() {
		// Betrag: sqrt(real² + imag²)
		return Math.sqrt(this.real * this.real + this.imag * this.imag);
	}

	public ComplexNumber sqrt() throws InvalidInputForSquareRoot {
		if (this.imag != 0.0d)
			throw new InvalidInputForSquareRoot();

		// In Polakoordinatenform umwandeln und die wurzel ziehen
		double phi = Math.atan(this.real / this.imag);
		double sqrabs = Math.sqrt(this.absoluteValue());

		// Frei nach: sqrt(c) = sqrt(abs(c)) * (cos(phi/2) + i*sin(phi/2))
		// und real = sqrt(abs(c)) * cos(phi/2) und imag = sqrt(abs(c)) * sin(phi/2)
		return new ComplexNumber(sqrabs * Math.cos(phi / 2), sqrabs * Math.sin(phi / 2));
	}

	public String toString() {
		// bitte nicht veraendern (wegen Backend-Kontrolle)
		return real + " + " + imag + "*i";
	}

	public static void main(String[] args) {
		// hier bitte Quelltext einfuegen
		//TODO: testen
	}

}
