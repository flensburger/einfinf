package einfinf6;

public class Polynomial {
	private int degree; // highest power of x
	private double[] coeffs; // Array with coefficients, all coefficients
								// [0..degree] are necessary

	public Polynomial(double[] c) {
		this.coeffs = c;
		degree = c.length - 1;
	}

	public int getDegree() {
		return degree;
	}

	public double[] getAllCoefficients() {
		return coeffs;
	}

	public double getCoefficient(int i) {
		return coeffs[i];
	}

	public String toString() {
		String ret = "";

		for (int i = coeffs.length - 1; i >= 0; i--) {
			if (i == 0) {
				ret += coeffs[i];
			} else {
				ret += coeffs[i] + "x^" + i + " + ";
			}
		}

		return ret;
	}

	public double evaluate(double x) {
		double previousResult = 0;
		for(int i = coeffs.length-1; i >= 0; i--){
			previousResult = previousResult*x + coeffs[i];
		}
		return previousResult;
	}

	public Polynomial differentiate() {
		double[] ret = new double[coeffs.length - 1];

		for (int i = 1; i < coeffs.length; i++) {
			ret[i - 1] = coeffs[i] * i;
		}
		return new Polynomial(ret);
	}

	public static void main(String[] args) {
		
		double[] inhalt = {2.0, 0.0, 1.0};
		Polynomial poly = new Polynomial(inhalt);
		System.out.println(poly.evaluate(2));
		
		
	}
}