package einfinf6;
import java.util.Arrays;

public class Food implements Comparable<Food> {
    
    private static final String[] TYPES = {"Apple", "Pear", "Cookie"};
    
    private String type;
    private int calories;
    
    public Food(String type, int calories) {
    	this.type = type;
    	this.calories = calories;
    }
    public String toString(){
         return type + ": " + calories + " Kalorien";
    }
    public int compareTo(Food o) {
    	return this.getCompareInteger()-o.getCompareInteger();
    }
    
    public String getType() {
        return type;

    }
    
    public int getCalories() {
       return calories;
    }
    
    public int getCompareInteger(){
    	return type.length()*calories;
    }
    
    public static Food[] createSortedRandomList(int n) {
        
        Food[] array = new Food[n];
        for(int i = 0; i < n; i++){
        	
        	int randomFood =(int)Math.round(Math.random()*2);
        	int randomCalo =(int)Math.round(Math.random()*100);
        	System.out.println(randomFood +" | " + randomCalo);
        	
        	array[i] = new Food(TYPES[randomFood], randomCalo);
        }
        Arrays.sort(array);
        return array;
    }

    public static void main(String[] args) {
        Food[] meeb = createSortedRandomList(5);
        System.out.println(Arrays.toString(meeb));
    }

}
