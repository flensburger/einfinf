package einfinf6;
public class RatNumber implements Comparable<RatNumber>{
	private int num = 0;
	private int denom = 1;
        
	public RatNumber(int n, int d){
		
		// Gefordertes Abfangen von Division durch 0
		if (d == 0){
			System.out.println("Falsche Eingabe.");
		} else {
			// Setzen und Normalisieren von Eingaben
			
			this.num = n;
			this.denom = d;
			this.normalize();
			
			// Minus-Zeichen von Nenner in den Z�hler schieben.
			if (this.denom < 0){
				this.denom *= -1;
				this.num *= -1;
			}
		}
		
	}
	// Copy Constructor - should be private
	public RatNumber(RatNumber n){
		this(n.getNum(), n.getDenom());
	}
	
	public void normalize(){
		// K�rzen nach Algorhythmus
		int ggT = getggT(num, denom);
		num /= ggT;
		denom /= ggT;
	}
	
	public void expand(int expander){
		// Erweitern mit expander
		this.num *= expander;
		this.denom *= expander;
	}
	
	public void toReciprocal(){
		// Kehrwert bilden
		int temp = this.num;
		this.num = this.denom;
		this.denom = temp;
	}
	
	//Funktion zum GGT
	public int getggT(int a, int b){
		if (b == 0){
			return a;
		} else {
			System.out.println( a + " / " + b + " = " + (int)(a/b) + " R " + a%b);
			return getggT(b, a%b);
		}
	}
	
	public int getNum() {
		return num;
	}

	public int getDenom() {
		return denom;
	}
	
	public int compareTo(RatNumber n){
		if (n.getNum()  == this.getNum() && n.getDenom() == this.getDenom()){
			return 0;
		}
		if (n.getNum()/n.getDenom() < this.getNum()/this.getDenom()){
			return 1;
		}
		return -1;
	}
	public RatNumber add(RatNumber r) {
		RatNumber ret = new RatNumber(this);
		// Erweitern
		int retExpander = ret.getDenom();
		ret.expand(r.getDenom());
		r.expand(retExpander);
		// Addieren und normalisieren
		ret.num += r.num;
		ret.normalize();
		return ret;

	}
	public RatNumber div(RatNumber r) {
		// Mit Kehrwert multiplizieren
		RatNumber ret = new RatNumber(this);
		r.toReciprocal();
		ret.num *= r.getNum();
		ret.denom *= r.getDenom();
		ret.normalize();
		
		
		return ret;
	}	
	public String toString() {
		return num + "/" + denom;
	}

	public static void main(String[] args) {
		RatNumber rat = new RatNumber(21,-14);
		rat.getggT(92911743, 5603373);
	}

}