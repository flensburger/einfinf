Gesucht:
pred      -- Vorgänger
sub       -- Subtraktion
mult      -- Multiplikation
div       -- Ganzzahldivision
mod       -- ganzzahliger Rest
even      -- Prädikat der Geradzahligkeit


type Nat
import Bool
operators
        0:    -> Nat                 -- Konstruktor
        suc:  Nat -> Nat             -- Konstruktor, Nachfolger
        add:  Nat x Nat -> Nat       -- Addition
        =*:   Nat x Nat -> Bool      -- Gleichheitsrelation
        >:    Nat x Nat -> Bool      -- Vergleich
		pred: Nat -> Nat			 -- Vorgänger
		sub:  Nat x Nat -> Nat		 -- Subtraktion
		mult: Nat x Nat -> Nat		 -- Multiplikation
		mod:  Nat x Nat-> Nat		 -- ganzzahliger Rest
		even: Nat -> Bool			 -- Prädikat der Geradzahligkeit	

axioms  x, y: Nat
        add(x, 0)          = x
        add(x, suc(y))     = suc (add(x, y))

        0 =* 0             = true
        0 =* suc (x)       = false
        suc (x) =* 0       = false
        suc (x) =* suc (y) = x =* y

        0 > 0           = false
        suc (x) > 0     = true
        x > suc (y)     = ( not(x =* y) and (x > y) )
        
        sub(x, 0)		= x
        
        mult(x, 0)		= 0
        mult(x, 1) 		= x
        
        div(x, 1) 		= x
        
        mod(x, 1)	 	= x
        
        even(suc(x)) = !even(x)
        
Preconditions:
		pred(x) : x > 0
        sub(x, y) : x > y
        div(x, y) : y != 0, x >= y
        mod(x, y) : y > 0
        