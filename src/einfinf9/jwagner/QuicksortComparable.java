package einfinf9.jwagner;

/*********************************************************
 * a) Ausgangsarray: {5, 3, 4, 8, 7, 1, 2} 1. Zerlegung {1, 2, 4, 8, 7, 5, 3} 2.
 * Zerlegung {1, 2, 3, 8, 7, 5, 4} 3. Zerlegung {1, 2, 3, 4, 7, 5, 8} 4.
 * Zerlegung {1, 2, 3, 4, 7, 5, 8} 5. Zerlegung {1, 2, 3, 4, 5, 7, 8}
 * 
 * b) Der schlechteste Fall ist immer eine sortierte oder invers sortierte
 * Folge, da dort bei jeder Zerlegung ein neues Pivot-Element gewählt werden
 * muss. Somit werden die maximale Anzahl an Vergleichen benötigt.
 * 
 * Der Aufwand für jede Sortiertung hängt vom Pivot-Element und von der
 * ursprünglichen Sortierung der Reihe ab. 
 * 
 * d) Ausgabe main: unsortiert 8:14 4:2 6:23 23:45 sortiert 4:2 6:23 8:14 23:45
 *
 ********************************************************/

public class QuicksortComparable {

	public static <T extends Comparable<T>> void quicksort(T[] a) {
		_quicksort(a, 0, a.length - 1);

	}

	static <T extends Comparable<T>> void _quicksort(T[] a, int l, int r) {
		if (r > l) {

			// Teilfelder durchtauschen...
			int m = partition(a, l, r);

			// ...danach Quicksort auf beide Teilfelder anwenden
			_quicksort(a, l, m - 1);
			_quicksort(a, m + 1, r);
		}
	}

	static <T extends Comparable<T>> int partition(T[] a, int l, int r) {

		T pivot = a[r]; // Das letzte immer als Pivot Element wählen
		T temp;
		int i = l, j = r - 1;
		do {

			// Links ein Element Größer als das Pivot suchen
			while (a[i].compareTo(pivot) < 0) {
				i++;
			}

			// Rechts ein Element kleiner als das Pivot suchen
			while (j > 0 && a[j].compareTo(pivot) > 0) {
				j--;
			}

			// Vertauschen
			temp = a[i];
			a[i] = a[j];
			a[j] = temp;

		} while (i < j);

		// Das Pivot Element mit vertauschen
		a[j] = a[i];
		a[i] = a[r];
		a[r] = temp;

		// Den index des neuen Pivot Elements ausgeben
		return i;
	}

	public static void main(String[] args) {
		Clock[] clocks = { new Clock(32, 14), new Clock(4, 2), new Clock(54, 23), new Clock(23, 45) };

		System.out.println("unsortiert");
		for (Clock c : clocks) {
			System.out.println(c.toString());
		}

		quicksort(clocks);

		System.out.println("sortiert");
		for (Clock c : clocks) {
			System.out.println(c.toString());
		}
	}
}
