package einfinf9.jwagner;

/*********************************************************
 * 
 * a) SelectionSort ist ien nicht stabiles Sortierverfahren, bei dem jedes
 * Element mit allen weiteren Indizees verglichen wird und mit dem Kleinsten,
 * welches gefunden wurde, vertauscht. Durchschnittlich n²/2 Vergleiche.
 * 
 * InsertionSort hat eine sortiertte Teilfolge und eine unsortierte Teilfolge.
 * Jedes Element der unsortierten Teilfolge wird in die sortierte Teilfolge
 * eingefügt. Dabei werden alle anderen Array-Elemente um Eins nach Rechts
 * verschoben. max n²/2 Vergleiche, im mittel n²/4 Vergleiche, best. n Vergleiche
 - im Mittel n²/8 Vertauschungen.
 * 
 * b) SelectionSort ist nicht stabil, da durch das Vertauschen der Elemente die
 * Reihenfolge verändert werden kann.
 * 
 * InsertionSort ist stabil, da beim Durchgehen und Einfügen des Arrays die
 * Elemente nicht vertauscht werden, bzw. in der gleichen Reihenfolge wieder
 * einsortiert werden.
 * 
 * c) Die Performance ändert sich nur bei den Vergleichen, da das Suchen erstmal
 * nichts mit dem Sortieren an sich zu tun hat. Die Vergleiche werden dabei
 * schneller, das eigentliche Vertauschen ist gleichschnell, da nur
 * Speicherreferenzen getauscht werden.
 * 
 ********************************************************/

public class InsertionSelectionSort {
	public static boolean ready(){
		return true;
	}
}