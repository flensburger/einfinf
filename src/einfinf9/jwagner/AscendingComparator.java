package einfinf9.jwagner;

import java.util.Comparator;

class AscendingComparator<T extends Comparable<T>> implements Comparator<T> {
	  public int compare(T a, T b) {
	    return a.compareTo(b);
	  }
	}
