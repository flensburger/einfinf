package einfinf9.mwallewein;

import java.util.Arrays;
import java.util.Comparator;

/*********************************************************
 * a) 0. Mischen {5, 3, 4, 7, 1, 2} 1. Mischen {3, 5, 4, 7, 1, 2} 2. Mischen {3,
 * 4, 5, 7, 1, 2} 3. Mischen {3, 4, 5, 1, 7, 2} 4. Mischen {3, 4, 5, 1, 2, 7} 5.
 * Mischen {1, 2, 3, 4, 5, 7}
 * 
 * b) In dem Algorithmus der Vorlesung wird bei jedem Funktionsaufruf ein neues
 * Array erstellt und somit Speicher reserviert. In diesem Algorithmus werden
 * nur die Referenzen von 2 Arrays durchgereicht
 * 
 * c) Mergesort in dieser Ausführun ist stabil, da beim rüberkopieren und
 * vergleichen der Werte bei gleichheit diese wieder in gleicher Reihenfolge in
 * das Ursprungsarray kopiert werden
 * 
 * 
 ********************************************************/

public class MergesortComparable {

	// Hilfsmethoden für rekursives Sortieren durch Mischen
	private static <T> void _mergeSort(T a[], T copy[], int start, int end,
			Comparator<T> c) {
		if (start < end) {
			int mid = (start + end) / 2;
			_mergeSort(a, copy, start, mid, c);
			_mergeSort(a, copy, mid + 1, end, c);
			merge(a, copy, start, mid, end, c);
		}
	}

	private static <T> void merge(T a[], T copy[], int start, int m, int end,
			Comparator<T> c) {
		int i = 0, j = start, k;
		while (j <= m)
			copy[i++] = a[j++];
		i = 0;
		k = start;
		while (k < j && j <= end) {
			if (c.compare(copy[i], a[j]) < 0)
				a[k++] = copy[i++];
			else
				a[k++] = a[j++];
		}
		while (k < j)
			a[k++] = copy[i++];
	}

	public static <T> void mergeSort(T[] a, Comparator<T> c) {
		// a.clone(), da kein neues Array erstellt werden kann
		_mergeSort(a, a.clone(), 0, a.length - 1, c);
	}

	public static void main(String[] args) {
		String[] a = { "test", "32q4", "fsdf", "asdas" };
		mergeSort(a, new DescendingComparator<String>());
		System.out.println("result: " + Arrays.toString(a));
	}
}
