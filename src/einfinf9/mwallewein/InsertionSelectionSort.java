package einfinf9.mwallewein;

/********************************************************* 

a)
SelectionSort:						
 - Für jeden index im Array wird in allen weiteren indizes der kleinste
   Eintrag gesucht und mit dem aktuellen index vertauscht
 - max. n-1 Vertauschungen für Array mit Länge n wenn absteigend sortiert
 - n²/2 Vergleiche 
 - nicht stabil
 
InsertionSort:
 - Für jeden index im Array den nächsten kleineren nehmen und in die 
   bereits sortierte Teilfoglge einfügen 
 - Alle weiteren Elemente um 1 nach rechts verschieben
 - max n²/2 Vergleiche, im mittel n²/4 Vergleiche, best. n Vergleiche
 - im Mittel n²/8 Vertauschungen
 - stabil
  
  
b)
SelectionSort: nicht stabil, weil
 - durch Vertauschen der Elemente kann die Reihenfolge von gleichen
   Elementen geändert werden

InsertionSort: stabil, weil
 - Die Elemente in der gleichen Reihnenfolge wieder von vorne in das Array
   einsortiert werden
   
c)
Bei rechenintensiven Vergleichen (z.B. Zeichenketten) verbessert sich die 
Sortierperformance bei den Vergleichen, die Anzahl der Vertauschungen bleibt gleich,
wobei nur Referenzen vertauscht werden

********************************************************/

public class InsertionSelectionSort {

}
