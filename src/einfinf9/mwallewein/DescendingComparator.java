package einfinf9.mwallewein;

import java.util.Comparator;


class DescendingComparator<T extends Comparable<T>> implements Comparator<T> {
	  public int compare(T a, T b) {
	    return b.compareTo(a);
	  }
	}
