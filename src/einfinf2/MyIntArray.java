package einfinf2;

public class MyIntArray {

	public static void main(String[] args) {
		// Zufälliges Array erzeugen
		int[] randomNumbers = MyIntArray.createRandom(13);

		// Dieses ausgeben
		System.out.println(MyIntArray.toString(randomNumbers));

		// Index des kleinsten Werts ausgeben
		System.out.printf("Index des kleinsten Wertes aus RandomNumbers: %d\n",
				posMin(randomNumbers));

		System.out.printf("Letzer Wert: %d, Erster Wert: %d\n",
				randomNumbers[0], randomNumbers[randomNumbers.length - 1]);

		System.out.println("Swappe ersten und letzten Wert");
		MyIntArray.swap(randomNumbers);

		System.out.printf("Letzer Wert: %d, Erster Wert: %d\n",
				randomNumbers[0], randomNumbers[randomNumbers.length - 1]);
	}

	public static int[] createRandom(int n) {

		// Feldgröße aus Parameter
		int[] randomNumbers = new int[n];
		int minRandomRange = 5;
		int maxRandomRange = 99;

		// Alle Elemente des Array mit zufallswerten zwischen
		// min/maxRandomRange befüllen
		for (int i = 0; i < n; i++) {
			
			// Format zum Generieren von Zufallszahlen in einer best.
			// Range:
			// Min + (int)(Math.random() * ((Max - Min) + 1))
			// nach Beispiel: 5 + Zufallszahl(0-94)
			randomNumbers[i] = minRandomRange
					+ (int) (Math.random() * ((maxRandomRange - minRandomRange) + 1));
		}

		return randomNumbers;
	}

	public static String toString(int[] a) {
		String arrayString = "";

		// Jedes Element durchgehen...
		for (int i = 0; i < a.length; i++) {
			// ...hinten an den String anhängen...
			arrayString += a[i];

			// ...und wenn es nicht grade des letzte Element ist
			// dann " ," anfügen
			if (i != a.length - 1)
				arrayString += ", ";
		}

		return arrayString;
	}

	public static int posMin(int[] a) {
		// Um alle Elemente mit <= vergleichen zu können
		// fängt man bei MAX_VALUE an
		int minValue = Integer.MAX_VALUE;

		// Erst den kleinsten Wert suchen...
		for (int i = 0; i < a.length; i++) {
			if (a[i] <= minValue)
				minValue = a[i];
		}

		// ...und von diesem dann das erste Vorkommen zurück-
		// geben
		for (int i = 0; i < a.length; i++) {
			if (a[i] == minValue)
				return i;
		}

		return -1;
	}

	public static void swap(int[] a) {

		// Erstes Element zwischenspeichern,
		// ihm den Wert des letzten Elements geben und
		// dem letzten daraufhin den gespeicherten Wert zuweisen
		int temp = a[0];
		a[0] = a[a.length - 1];
		a[a.length - 1] = temp;
	}
}
