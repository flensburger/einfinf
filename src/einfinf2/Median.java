package einfinf2;

import java.util.Arrays;

public class Median {

	public static void main(String[] args) {

		// Testrahmen:
		// Beliebige Zahlen
		System.out.printf("median von %d, %d, %d => %d\n", 1, 2, 3,
				median(1, 2, 3));
		System.out.printf("median2 von %d, %d, %d => %d\n", 1, 2, 3,
				median2(1, 2, 3));

		System.out.printf("median von %d, %d, %d => %d\n", 123, 567, 42,
				median(123, 567, 42));
		System.out.printf("median2 von %d, %d, %d => %d\n", 123, 567, 42,
				median2(123, 567, 42));

		// Zwei Gleiche Zahlen
		System.out.printf("median von %d, %d, %d => %d\n", 1, 3, 1,
				median(1, 3, 1));
		System.out.printf("median2 von %d, %d, %d => %d\n", 1, 3, 1,
				median2(1, 3, 1));

		// Negative Zahlen
		System.out.printf("median von %d, %d, %d => %d\n", 6, -5, -7,
				median(6, -5, -7));
		System.out.printf("median2 von %d, %d, %d => %d\n", 6, -5, -7,
				median2(6, -5, -7));

	}

	public static int median(int a, int b, int c) {

		// Die Zahlen als Array speichern, durchsortieren
		// und das mittlere Element nehmen
		int[] numbers = { a, b, c };
		Arrays.sort(numbers);
		return numbers[1];

	}

	public static int median2(int a, int b, int c) {

		// Die Zahlen als Array speichern
		int[] numbers = { a, b, c };
		int prevIndex, nextIndex, median;

		// Von jeder Zahl den Vor- und Nachgänger mit der aktuellen
		// Zahl vergleichen, ob diese dazwischen liegt
		for (int i = 0; i < numbers.length; i++) {

			// Beim ersten Element wird der Vorgänger auf das letzte
			// Element gesetzt
			prevIndex = i - 1;
			if (prevIndex < 0)
				prevIndex = numbers.length + prevIndex;

			// Beim letzten Element wird der Nachfolger auf das erste
			// Element gesetzt
			nextIndex = i + 1;
			if (nextIndex > numbers.length - 1)
				nextIndex = 0;

			// Prüfen, ob es das mittlere Element im Array ist
			if ((numbers[i] >= numbers[prevIndex] && numbers[i] <= numbers[nextIndex])
					|| (numbers[i] <= numbers[prevIndex] && numbers[i] >= numbers[nextIndex])) {
				return numbers[i];
			}
		}

		// TODO: Fehlerhandling...
		return -1;
	}

}
