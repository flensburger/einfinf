package einfinf2;

public class TransformDual {

	public static void main(String[] args) {

		System.out.printf("Dezimalzahl %d in Dualsystem: %s\n", 13,
				transformDual(13));
		// Da dec > 0 fällt dieser Testfall weg
		// System.out.printf("Dezimalzahl %d in Dualsystem: %s\n", 0,
		// transformDual(0));
		System.out.printf("Dezimalzahl %d in Dualsystem: %s\n", 1,
				transformDual(1));
		System.out.printf("Dezimalzahl %d in Dualsystem: %s\n", 15,
				transformDual(15));
		System.out.printf("Dezimalzahl %d in Dualsystem: %s\n", 255,
				transformDual(255));
		System.out.printf("Dezimalzahl %d in Dualsystem: %s\n", 257,
				transformDual(257));
	}

	public static String transformDual(int dec) {
		String dual = "";

		// dec schrittweise durch zwei teilen,
		// dec immer > 0!
		while (dec > 0) {
			// Dualzahl von hinten auffüllen
			// mit dem Rest aus dec /2
			dual = dec % 2 + dual;

			dec = dec / 2;
		}

		return dual;

	}
}
