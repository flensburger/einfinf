package einfinf3;

public class PerfectMerge {

	public static void main(String[] args) {
		int[] a1 = { 1, 2, 3 };
		int[] a2 = { 4, 5, 6 };
		int[] c = interleave(a1, a2);// c enthält die Elemente 1,4,2,5,3,6

		int[] a = { 1, 2, 3, 4 };
		a = perfectMerge(a);

		// Für alle geraden zahlen mischen
		for (int i = 0; i < 100; i++) {
			if (i % 2 != 0)
				continue;

			System.out.printf("Size: %d -> %d mal gemischt\n", i,
					mergeNumber(i));
		}
		// System.out.println(mergeNumber(52));
		// perfectMerge(a) ergibt dann ein Feld mit den Elementen 1,3,2,4
	}
	
	public static int[] interleave(int[] a1, int[] a2) {
		// hier bitte Quelltext einreichen
		int[] interleaved = new int[a1.length + a2.length];

		// 0 für a1, 1 für a2
		int arrayIndex = 0;
		int rowIndex = 0;

		// Anhand des neuen Arrays iterieren und den Index
		// merken, mit dem man auf die alten Arrays zugreift
		for (int i = 0; i < interleaved.length; i++) {
			if (arrayIndex == 1) {
				interleaved[i] = a2[rowIndex];
				arrayIndex = 0;
				rowIndex++;
			} else {
				interleaved[i] = a1[rowIndex];
				arrayIndex = 1;
			}
		}

		return interleaved;

	}

	public static int[] perfectMerge(int[] a) {

		// Zu testzwecken erweitert...
		if (a.length < 2 || a.length % 2 != 0)
			return a;

		// Zwei neue Arrays generieren und mit interleave
		// zusammenführen
		int seperateIndex = a.length / 2;

		int[] a1 = new int[seperateIndex];
		int[] a2 = new int[seperateIndex];

		for (int i = 0; i < a.length; i++) {
			if (i < seperateIndex)
				a1[i] = a[i];
			else
				// Wenn i >= seperateIndex muss wieder
				// bei 0 angefangen werden
				a2[i - seperateIndex] = a[i];
		}

		return interleave(a1, a2);

	}

	public static int mergeNumber(int n) {

		// Zwei Arrays generieren, eines wird gemischt und
		// das andere bleibt zwecks Vergleich beim alten
		int[] a = new int[n];
		int[] b = new int[n];

		// Mit den Werten von 1 bis 32 für ein normales
		// Kartenspiel
		for (int i = 0; i < n; i++) {
			a[i] = randomNumber(1, 32);
			b[i] = a[i];
		}

		// Solange mischen, bis a wieder gleich b
		int i = 0;
		do {
			a = perfectMerge(a);
			i++;
		} while (!compareArray(a, b));

		return i;
	}

	// Hilfefunktionen

	public static int randomNumber(int min, int max) {
		return min + (int) (Math.random() * ((max - min) + 1));
	}

	public static boolean compareArray(int[] a1, int[] a2) {
		if (a1.length != a2.length)
			return false;

		// jedes Element vergleichen, sobald sich eins unterscheidet
		// sind die Arrays nicht gleich
		for (int i = 0; i < a1.length; i++) {
			if (a1[i] != a2[i])
				return false;
		}

		return true;
	}
	
}
