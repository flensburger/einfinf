package einfinf3;

public class AmicableNumbers {

	public static void main(String[] args) {
		
		int[][] amicablePairs = amicablePairs(5);
		for(int i = 0; i < amicablePairs.length; i++)
		{
			System.out.printf("Paar[%d] = [%d], [%d]\n", 
					i,
					amicablePairs[i][0],
					amicablePairs[i][1]);
		}
		
	}
	
	public static int[][] amicablePairs(int n) {
		// Matrix mit n Zeilen und 2 Spalten für die
		// befreundeten Zahlen
		int[][] amicablePairs = new int[n][2];

		// Anzahl bereits gefundener Paare
		int pairCount = 0;

		// Für eine feste natürliche Zahl n sei 
		// x = f * 2^n - 1, y = f * 2^{n-k} - 1 und z = f^2 * 2^{2n-k} - 1
		// mit f = 2^k+1 und n > k > 0.
		// Wenn x, y und z Primzahlen sind, dann sind die beiden Zahlen 
		// a = 2^n * x * y und b = 2^n * z befreundet.
		
		// k = 1 und f = 3 für den Satz von Thabit
		int k = 1, f = 3, x, y, z, a, b;
		
		// Zähler für jedes n in der Formel
		int i = 2;

		// So lange suchen bis n befreundete Paare gefunden
		while(pairCount < n)
		{
			for(int j = 1; j < i; j++)
			{
				f = pow(2, j) + 1;
				
				// Satz von Euler
				x = f * pow(2, i) - 1;
				y = f * pow(2, (i - j)) - 1;
				z = (f*f) * pow(2, 2 * i - j) - 1;
				
				// Prüfen ob alle 3 Primzahlen sind
				if(isPrime(x)
					&& isPrime(y)
					&& isPrime(z))
				{
					a = pow(2, i) * x * y;
					b = pow(2, i) * z;
					
					if(a < b)
					{
						amicablePairs[pairCount][0] = a;
						amicablePairs[pairCount][1] = b;
					}
					else
					{
						amicablePairs[pairCount][0] = b;
						amicablePairs[pairCount][1] = a;
					}
					
					pairCount++;
					break;
				}
			}
			i++;
		}
		
		return amicablePairs;
	}

	public static int pow(int base, int exp) {
		if (exp == 0)
			return 1;

		int result = base;
		
		while(exp > 1)
		{
			result *= base;
			exp--;
		}
		
		return result;
		
		//return base * (pow(base, exp - 1));
	}

	/**
	 * Prüft ob die eingegebene Zahl eine Primzahl ist
	 * 
	 * @param n
	 *            zu überprüfende Zahl
	 * @return true wenn Primzahl
	 */
	public static boolean isPrime(int n) {
		// alles unter 1 extra handlen, da sonst immer false zurückgegeben wird
		// in der Schleife unten
		if (n < 2)
			return false;

		// Bis zur Wurzel von n durchgehen und n durch diese Zahlen
		// teilen

		for (int i = 2; i * i <= n; i++) {
			// Wenn sich ein Rest bei irgendeiner Zahl ergibt
			// dann ist n keine Primzahl
			if (n % i == 0)
				return false;
		}

		// Wenn sich kein Rest ergeben hat, dann ist
		// n eine Primzahl
		return true;
	}
}
