package einfinf3;
/**************************************
in diesem Kommentar bitte Antwort zu Teil a) einfuegen

Der Algorithmus subtrahiert y von x.

Er terminiert nicht für y < 0

***********************************************/
public class Rek {

    public static int rek(int x, int y) {
    
    	if(x == y)
    		return 0;
    	
    	if(y == 0)
    		return x;
    	
    	if(y > 0)
    		return rek(x - 1, y - 1);
    	else
    		return rek(x + 1, y + 1);

    }

    public static int iter(int x, int y) {
    
    	if(x == y)
    		return 0;
    	
    	while(y != 0)
    	{
    		
    		
    		if(y < 0)
    		{
    			y++;
    			x++;
    		}
    		else
    		{
    			y--;
    			x--;
    		}
    	}
    	
    	return x;

    }
  
   public static void main(String[] args) {
     // hier bitte Testrahmen einfuegen
	   System.out.println(rek(3, 6));
	   System.out.println(iter(3, -6));
	   
	   System.out.println(iter(3, 6));
	   System.out.println(rek(3, -6));
   }
}